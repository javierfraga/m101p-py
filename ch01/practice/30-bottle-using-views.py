import bottle

@bottle.route( '/' )
def home_page():
    mythings = [ 'apple' , 'orange' , 'banana' , 'peach' ]
    # return bottle.template( 'hello_world' , username='javier' , things=mythings )
    return bottle.template( 'hello_world' , {'username':'fraga' , 'things':mythings} ) # python always sends vars above as dict, thus, equivilent

bottle.debug(True)
bottle.run( host='localhost' , port=8080 )
    
