#!/usr/bin/env python
import pymongo

connection = pymongo.MongoClient( 'mongodb://localhost' )
db         = connection.students
grades     = db.grades

def find():
    print( 'running functions find()' )

    query = { 'type' : 'homework'  } 
    display = { 'student_id' : 1 , 'score' : 1 , 'type' : 1  }
    sort = { 'student_id' : 1 , 'sort' : 1 }

    try:
        cursor = grades.find( query , display )
        cursor.sort([  ( 'student_id' , pymongo.ASCENDING ) , ( 'score' , pymongo.ASCENDING )  ])
        sanity = 0
        student_id = 0
        for item in cursor:
           if ( item['student_id'] == student_id ):
               print( item )
               student_id += 1
               lowest_grade_id = item['_id']
               print( lowest_grade_id )
               result = grades.delete_one( { '_id' : lowest_grade_id } )
           # sanity += 1
           # if ( sanity > 20 ):
                # break
    except Exception as e:
        print( 'Unexpected error:', type(e), e )

    # sanity = 0
    # for item in cursor:
        # print( item )
        # sanity += 1
        # if ( sanity > 10 ):
            # break

if __name__ == '__main__':
    find()
