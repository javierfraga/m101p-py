#!/usr/bin/env python
import pymongo

connection = pymongo.MongoClient( 'mongodb://localhost' )
db         = connection.school
students     = db.students

def find():
    print( 'running functions find ()' )

    query = { 'scores' : { '$exists' : 'true' } }

    try:
        cursors = students.find( query )
        for item in cursors:
            # print ( item['scores'] )
            for score in item['scores']:
                # print(score)
                try:
                    current
                except NameError:
                    print ('was not defined')
                else:
                    previous = current
                if 'homework' in score['type']:
                    # print(score)
                    current = score['score']
            print( 'here is current: ' , current )
            print( 'here is previous: ' , previous )
            if current-previous > 0:
                print( 'here is the lowest' , previous )
                choice = previous
            else:
                print( 'here is the lowest: ' , current )
                choice = current
            print( item )
            students.update_one( { '_id' : item['_id'] } , { '$pull' : { 'scores' : { 'type' : 'homework' , 'score' : choice } } } )
            del current
            del previous
            print( '----------' )
    except Exception as e:
        print( 'Unexpected error: ', type(e) , e )

if __name__ == '__main__':
    find()
