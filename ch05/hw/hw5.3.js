db.grades.aggregate([
    { $unwind : '$scores' }
    ,
    { $match : 
        {
            $or : 
            [
                { 'scores.type' : 'exam' },
                { 'scores.type' : 'homework' }
            ]
        }
    }
    ,
    { $group :
        {
            _id : 
            {
                'student' : '$student_id',
                'class' : '$class_id'
            }
            ,
            'average' : 
            {
                '$avg' : '$scores.score' 
            }
        }
    }
    ,
    { $group :
        {
            _id :
            {
                class : '$_id.class'
            }
            ,
            'class-average' :
            {
                '$avg' : '$average'
            }
        }
    }
    ,
    { $sort :
        {
            'class-average' : 1
        }
    }
])
