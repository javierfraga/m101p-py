db.zips.aggregate([
    {
        $project : 
        {
            'first-char' : 
            {
                $substr : [ '$city' , 0 , 1 ]
            }
        }
    }
    ,
    {
        $sort :
        {
            'first-char' : 1
        }
    }
])

db.zips.aggregate([
    {
        $match :
        {
            'city' : /^\d/
        }
    }
    ,
    {
        $group :
        {
            _id : 0
            ,
            'sum' :
            {
                $sum : '$pop'
            }
        }
    }
])
